import { useEffect, useState } from "react";
import { CountryType, LanguageType } from "../../interfaces/interfaces";


export default function Country({code, name, phone, capital, currency, emoji, languages}: CountryType){
    const [language, setLanguage] = useState<Array<LanguageType>>([]);

    const getLanguages = async () => {
        const response = await fetch("https://countries.trevorblades.com/", {method: "POST", headers: {'Content-Type': 'application/json'}, 
        body: JSON.stringify({query: `{country(code: "${code}") {languages {code, name}}}`})});
        const data = await response.json();
    
        setLanguage(data.data.country.languages);
      }

      useEffect(() => {
        getLanguages();
      }, [])

    return (
        <div className="w-2/5 my-0 mx-auto mt-10 first:mt-2">
            <div className="flex mb-2">
                <div className="basis-1/4 text-lg font-bold">
                    {code}
                </div>

                <div className="basis-1/2 font-extrabold text-2xl">
                    {name}
                </div>

                <div className="basis-1/4 text-xl text-right">
                    {emoji}
                </div>
            </div>

            <div className="flex">
                <div className="basis-1/2">
                    <ul>
                        <li><strong>Předvolba</strong>: {phone}</li>
                        <li><strong>Měna</strong>: {currency}</li>
                        <li><strong>Hlavní město</strong>: {capital}</li>
                    </ul>
                </div>

                <div className="basis-1/2 ml-8">
                    {language.map((lang, index) => <p key={index}>{lang.code} – {lang.name}</p>)}
                </div>
            </div>
        </div>
    )
}