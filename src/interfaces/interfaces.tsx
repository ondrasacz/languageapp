export interface CountryType {
    code: string,
    name: string
    phone: string,
    capital: string,
    currency: string,
    languages?: Array<LanguageType>,
    emoji: string
}

export interface LanguageType {
    code: string,
    name: string
  }