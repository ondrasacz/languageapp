import { useEffect, useState } from "react";
import Country from "./components/country";
import { CountryType } from "./interfaces/interfaces";

export default function App() {
  const [countries, setCountries] = useState<Array<CountryType>>([]);
  const [pageNumber, setPageNumber] = useState<number>(0);
  const countiesOnPage:number = 6;

  //funkce by mohly být rozděleny do jiných souborů => jedná se ale o jednoduchou aplikaci, a tak jsem je nechal zde
  const fetchData = async () => {
    const response = await fetch("https://countries.trevorblades.com/", {method: "POST", headers: {'Content-Type': 'application/json'}, 
    body: JSON.stringify({query: `{countries {code, name, native, phone, capital, currency, emoji, emojiU}}`})});
    const data = await response.json();

    setCountries(data.data.countries);
  };

  //Rozhodl jsem se data stahovat vždy při mountnutí componenty, proto jsem zvolil useEffect hook
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="flex flex-wrap">
        {/* Nechtěl jsem zbytečně využívat react-router-dom pro routování na nové stránky, a tak jsem se rozhodl to vyřešit tímto matematickým způsobem s využitím useState hooku  */}
        {countries.slice(pageNumber * countiesOnPage, ((pageNumber + 1) * countiesOnPage) > (countries.length - 1) ? countries.length : (pageNumber + 1) * countiesOnPage).map((country, index) => <Country code={country.code} name={country.name} phone={country.phone} capital={country.capital} currency={country.currency} emoji={country.emoji} key={index} />)}
      </div>

      <div className="w-2/5 my-6 mx-auto flex">
        <button className="bg-slate-700 rounded-lg p-4 text-zinc-50 text-xl hover:shadow-8xl hover:shadow-red-600" onClick={() => setPageNumber(pageNumber <= 0 ? 0 : pageNumber - 1)}>
          Přejít na předchozí stránku
        </button>

        <button className="bg-slate-700 rounded-lg p-4 ml-12 text-zinc-50 text-xl hover:shadow-8xl hover:shadow-red-600" onClick={() => setPageNumber((pageNumber + 1) * countiesOnPage > (countries.length - 1) ? pageNumber : pageNumber + 1)}>
          Přejít na další stránku
        </button>
      </div>
    </div>

  );
}